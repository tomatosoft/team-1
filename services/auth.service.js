var authService = function () {

    function deletePassword(user) {
        var User = JSON.parse(JSON.stringify(user));
        delete User.password;
        return User;
    }

    function passwordGenerator() {
        var password = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 8; i++) {
            password += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return password;
    }

    return {
        deletePassword: deletePassword,
        passwordGenerator: passwordGenerator
    };
};

module.exports = authService;
