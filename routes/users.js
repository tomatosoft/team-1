var express = require('express');
var router = express.Router();
var User = require('../models/user');
var bcrypt = require('bcrypt');
var ObjectId = require('mongoose').Types.ObjectId;

router.get('/all/:pageNum', function(req, res, next) {
    getUsersPages({}, req.params.pageNum, function(err, data) {
        if (err) {
            next(err);
        }

        res.send(data);
    })
});

router.get('/followers/:username/:pageNum', function(req, res, next) {
    User.findOne({username: req.params.username}, function(err, user) {
        getUsersPages({'following': ObjectId(user._id)}, req.params.pageNum, function(err, page) {
            res.send(page);
        })
    })
});

router.get('/following/:username/:pageNum', function(req, res, next) {
    User.findOne({username: req.params.username}, function(err, user) {
        getUsersPages({'followers': ObjectId(user._id)}, req.params.pageNum, function(err, page) {
            res.send(page);
        })
    })
});

router.get('/search/:userInput/:pageNum', function(req, res, next) {
    var userInput = new RegExp('^.*' + req.params.userInput + '.*$', 'i');
    var queryArray = [
        {username: userInput},
        {first_name: userInput},
        {last_name: userInput},
        {email: userInput}
    ];

    getUsersPages({$or: queryArray}, req.params.pageNum, function(err, data){
        if (err) {
            console.log(err);
            next(err);
        }

        res.send(data);
    });
});

router.get('/:username', function(req, res, next) {
    var visitorId = req.query.visitorId;

    User.findOne({username: req.params.username}, function(err, user) {
        if (err) {
            console.log(err);
            next(err);
        }

        User.findOne({'followers': visitorId + ''}, function (err, follower) {
            if (err) {
                console.log(err);
                next(err);
            }

            res.send({user: user, followedByVisitor: !!follower});
        });
    });
});

router.put('/', function(req, res, next) {
    parseInfo(req, function(err, info) {
        if (err) {
            return res.status(err.status).send({message: err.message});
        }

        User.findByIdAndUpdate(req.user.id, info, function(err, user) {
            res.send({message: 'Your data has been successfully changed.'});
        })
    });
});

router.put('/toggleFollow/:username', function (req, res, next) {
    var follower = req.body.follower;
    var followedName = req.params.username;

    User.findOne(
        {'username': followedName},
        {'followers': follower._id},
        function (err, followed) {
            if(followed.followers.length != 0) {
                unfollow(followed, follower, res);
            }
            else {
                follow(followed, follower, res);
            }
        }
    );
});

function getUsersPages(param, pageNum, cb) {
    var pageLimit = 10;
    var skipValue = pageLimit * (pageNum - 1);

    User.find(param, function (err, users) {
        if (err) {
            return cb(err);
        }

        var usersCount = users.length;
        var pagesCount = Math.ceil(usersCount / pageLimit);

        User.find(param)
            .skip(skipValue)
            .limit(pageLimit)
            .exec(function (err, data) {
                if (err) {
                    return cb(err);
                }

                return cb(null, {
                    users: data,
                    usersCount: usersCount,
                    pagesCount: pagesCount
                })
            });
    });
}

function unfollow(followed, follower, res) {
    User.findOneAndUpdate(
        {'_id': followed._id},
        {$pull: {followers: follower._id + ''}},
        {new: true},

        function (err, followedUser) {
            User.findOneAndUpdate(
                {'_id': follower._id + ''},
                {$pull: {following: followed._id + ''}},
                {new: true},

                function (err, followingUser) {
                    res.status(200);
                    res.end();
                }
            );
        }
    );
}

function follow(followed, follower, res) {
    User.findOneAndUpdate(
        {'_id': followed._id},
        {$push: {followers: follower._id + ''}},
        {new: true},

        function (err, followedUser) {
            User.findOneAndUpdate(
                {'_id': follower._id + ''},
                {$push: {following: followed._id + ''}},
                {new: true},

                function (err, followingUser) {
                    res.status(200);
                    res.end();
                }
            );
        }
    );
}

function parseInfo(req, cb) {
    var info = req.body;
    if (info.password) {
        if (!(info.password.current && info.password.new && info.password.repeat)) {
            return cb({status: 400, message: 'Missing required data.'})
        }

        bcrypt.compare(info.password.current, req.user.password, function (err, match) {
            if (!match) {
                return cb({status: 400, message: 'Incorrect current password.'});
            }

            if (info.password.new !== info.password.repeat) {
                return cb({status: 400, message: 'New passwords don\'t match.'});
            }

            info.password = bcrypt.hashSync(info.password.new, bcrypt.genSaltSync(10));

            return cb(null, info);
        });
    } else {
        return cb(null, info);
    }
}

module.exports = router;
