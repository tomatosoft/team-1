var express = require('express');
var token = require('../controllers/token.controller');
var router = express.Router();

router.route('/new')
    .post(token.newToken);

router.route('/check/:token')
    .post(token.checkToken);

module.exports = router;




