var express = require('express');
var router = express.Router();
var authController= require('../controllers/auth.controller.js')();

router.route('/register')
    .post(authController.registerUser);

router.route('/login')
    .post(authController.login);

router.route('/logout')
    .get(authController.logout);

router.route('/islogged')
    .get(authController.isLogged);

router.route('/recovery')
    .post(authController.recovery);

router.route('/active/:token')
    .get(authController.checkUser);

module.exports = router;
