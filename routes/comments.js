var express = require('express');
var router = express.Router();
var commentController = require('../controllers/comment.controller');

router.route('/:photo')
    .get(commentController.getComments);

router.route('/save')
    .post(commentController.saveComment);

module.exports = router;