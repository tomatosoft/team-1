var express = require('express');
var adminController = require('../controllers/admin.controller.js')();
var router = express.Router();

router.route('/users/:id')
    .get(adminController.getUserById)
    .put(adminController.updateUser)
    .delete(adminController.deleteUser);

router.route('/complain/photos')
    .get(adminController.complainPhoto);

router.route('/delete/:username/:album/:photo')
    .delete(adminController.deletePhoto);

module.exports = router;
