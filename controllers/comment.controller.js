var Comment = require('../models/comment');
var Album = require('../models/album');

var getComments = function (req, res) {
    var photoId = req.params.photo;
    Album
        .findOne(
            {'photos': {$elemMatch: {_id: photoId, status: {$ne: 'no-comment'}}}},
            function (err, data) {
                if (data) {
                    Comment
                        .find({'postedTo': photoId})
                        .populate('postedBy')
                        .exec(function (err, comments) {
                            res.send(comments);
                        })
                } else {
                    res.send([]);
                }
            });
};

var saveComment = function (req, res) {
    var comment = req.body;
    console.log('sdfsdfsdfsdfsdf');
    Album
        .findOne(
            {'photos': {$elemMatch: {_id: comment.postedTo}}},
            function(err, photo){
                if(!err && photo){
                    if(photo.status != 'no-comment'){
                        Comment.create(comment, function (err, comment) {
                            if (err)
                                res.status(400).send(err);
                            else
                                res.status(200).send('Comment successfully saved !!!');
                        })
                    }
                }
            });
};

module.exports = {
    getComments: getComments,
    saveComment: saveComment
};