var express = require('express');
var router = express.Router();
var User = require('../models/user');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcrypt');
var sendEmail = require('../controllers/email.controller.js');
var token = require('../services/token.service.js');
var authService = require('../services/auth.service.js')();

var adminController = function () {

    passport.use(new LocalStrategy(
        function (username, password, done) {

            User.findOne({
                username: username
            }, '+password', function (err, user) {
                if (err) {
                    return done(err);
                }

                if (!user) {
                    return done(null, null, {message: 'Incorrect username.'});
                }

                bcrypt.compare(password, user.password, function (error, isValid) {
                    if (error) {
                        return error;
                    }

                    if (!isValid) {
                        return done(null, null, {message: 'Incorrect password.'})
                    }

                    return done(null, user)
                })
            })
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user && user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findOne({_id: id}, '+password', function (err, user) {
            if (err) {
                return done(err);
            }

            return done(null, user);
        })
    });

    var registerUser = function(req, res, next){
        bcrypt.hash(req.body.password, 10, function (error, hash) {
            req.body.password = hash;

            User.create(req.body, function (err, user) {
                if (err) {
                    if (err.code === 11000) {
                        return res.status(400).send({message: 'This username is already in use.'});
                    } else {
                        return res.status(400).send({message: err.message});
                    }
                }
                res.send(user ? {user: authService.deletePassword(user)} : null);
            });
        });
    };

    var login = function (req, res, next) {
        passport.authenticate('local', function (err, user, info) {
            if (err) {
                return next(err);
            }

            if (!user) {
                return res.status(400).send({message: info.message});
            }

            req.logIn(user, function (err) {
                if (err) {
                    return next(err);
                }

                res.send({user: authService.deletePassword(req.user)});
            })
        })(req, res, next)
    };

    var logout = function (req, res, next) {
        req.logOut();
        res.end();
    };

    var isLogged = function (req, res, next) {
        res.send(req.user ? {user: authService.deletePassword(req.user)} : null);
    };

    var recovery = function (req, res, next) {
        User.findOne({email: req.body.email}, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.status(400).send({message: "user isn't found"});
            }
            var newPassword = authService.passwordGenerator();
            user.save(function (err) {
                if (err) {
                    return res.status(400).send({message: "Can't save user"});
                } else {
                    var tn = token.create({"email": user.email, "password": newPassword});
                    user.password = newPassword;
                    sendEmail.sendRecoveryPassword(user, tn, function (err) {
                        if (err) {
                            return next(err);
                        } else {
                            res.status(200).send(
                                {message: 'New password was sent to ' + user.email + '. Check Your email box.'}
                            );
                        }
                    });
                }
            })
        })
    };

    var checkUser = function (req, res, next) {
        token.check(req.params.token, function (result, err) {
            if (err) {
                res.status(400).send({message: err});
            } else {
                User.findOne({'email': result.email}, function (err, user) {
                    if(!user){
                        res.status(400).send('user not found!!');
                    }else {
                        bcrypt.hash(result.password, 10, function (error, hash) {
                            user.password = hash;
                            user.save(function (err) {
                                if (err) {
                                    res.status(400).send({message: "Can't save user"});
                                } else {
                                    res.status(200).redirect('/login');
                                }
                            })
                        });
                    }
                })
            }
        })
    };

    return {
        registerUser: registerUser,
        login: login,
        logout: logout,
        isLogged: isLogged,
        recovery: recovery,
        checkUser: checkUser
    };
};

module.exports = adminController;