(function () {
    angular
        .module('blog')
        .service('UsersService', UsersService);

    UsersService.$inject = ['$http', '$routeParams'];

    function UsersService($http, $routeParams) {
        return {
            getPage: getPage,
            getFollowers: getFollowers,
            getFollowing: getFollowing,
            search: search
        };

        function getPage(pageNumber){
            return $http.get('/api/users/all/' + pageNumber).then(function (data) {
                return data.data;
            })
        }

        function getFollowers(pageNumber){
            return $http.get('/api/users/followers/' + $routeParams.username + '/' + pageNumber)
                .then(function (data) {
                    return data.data;
                })
        }

        function getFollowing(pageNumber){
            return $http.get('/api/users/following/' + $routeParams.username + '/' + pageNumber)
                .then(function (data) {
                    return data.data;
                })
        }

        function search(pageNumber, userInput) {
            return $http.get('/api/users/search/' + userInput + '/' + pageNumber).then(function(data) {
                return data.data;
            })
        }
    }
})();

