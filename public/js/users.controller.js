(function() {
    angular
        .module('blog')
        .controller('UsersController', UsersController);

    UsersController.$inject = ['UsersService', 'Pagination', '$location'];

    function UsersController(UsersService, Pagination, $location) {
        var vm = this;

        vm.currentPage = 1;
        vm.users = [];
        vm.pagesCount = 0;
        vm.paginationList = [];

        vm.getPage = getPage;
        vm.search = search;

        initPagination();
        getPage(vm.currentPage, null, setPageData);

        function initPagination() {
            var routeParam = $location.url().split('/').pop();

            Pagination.setGetPageFunc(
                routeParam == 'followers' ? UsersService.getFollowers
                : routeParam == 'following' ? UsersService.getFollowing
                : UsersService.getPage
            )
        }

        function getPage(pageNum, data, cb){
            Pagination.run(pageNum, data, cb)
        }

        function setPageData(){
            vm.users = Pagination.getPageItems();
            vm.paginationList = Pagination.getPaginationList();
            vm.currentPage = Pagination.getCurrentPageNum();
        }

        function search(userInput) {
            if (userInput) {
                Pagination.setGetPageFunc(UsersService.search)
            } else {
                initPagination();
            }

            getPage(1, userInput, setPageData);
        }
    }
})();
