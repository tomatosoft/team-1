(function () {
    angular
        .module('blog')
        .controller('AdminController', AdminController);

    AdminController.$inject = ['User', 'FlashMessage', '$http', '$q', 'UsersService', 'Pagination'];

    function AdminController(User, FlashMessage, $http, $q, UsersService, Pagination) {
        var vm = this;
        vm.delete = _delete;
        vm.ban = ban;
        vm.search = search;
        vm.deletePhoto = deletePhoto;
        vm.getPage = getPage;
        vm.currentPage = 1;
        vm.users = [];
        vm.pagesCount = 0;
        vm.paginationList = [];

        getComplainPhotos();
        Pagination.setGetPageFunc(UsersService.getPage);
        getPage(vm.currentPage, null, setPageData);

        function getPage(pageNum, data, cb){
            return Pagination.run(pageNum, data, cb)
        }

        function setPageData(){
            vm.users = Pagination.getPageItems();
            vm.paginationList = Pagination.getPaginationList();
            vm.currentPage = Pagination.getCurrentPageNum();

            return $q.resolve();
        }

        /**
         * Delete User by id
         * @param id
         */
        function _delete(id) {
            vm.dialog = {
                title: 'Deleting user',
                body: '<h1>User with  ID \''+ id +'\' will be deleted</h1>',
                onConfirm: function () {
                    User.delete({id: id}, function () {
                        getPage(vm.currentPage).then(function () {
                            FlashMessage.create(FlashMessage.TYPE.SUCCESS, 'User (id = ' + id + ') was successfully deleted.')
                        });
                    })
                }
            };
            vm.dialogVisible = true;
        }

        /**
         * Ban and unban User by id
         * @param id
         */
        function ban(id) {
            User.getOne({id: id}, function (user) {
                user.status == 'active'
                    ? user.status = 'baned'
                    : user.status = 'active';
                User.update({id: user._id}, {status: user.status}, function () {
                    getPage(vm.currentPage).then(function () {
                        FlashMessage.create(FlashMessage.TYPE.INFO, 'User ' + user.username + ' status: ' + user.status);
                    })
                });
            });
        }

        function deletePhoto(photo) {
            $http.delete('/api/admin/delete/'+photo.postedBy.username +'/'+ photo.album_id+'/'+photo.filename)
                .then(function() {
                    getComplainPhotos();
                    FlashMessage.create(FlashMessage.TYPE.SUCCESS, 'PHOTO DELETED' );
                })
        }

        function getComplainPhotos() {
            return $http.get('/api/admin/complain/photos').then(function(res) {
               vm.albums = res.data.complain;
            })
        }

        function search(userInput){
            if (userInput) {
                Pagination.setGetPageFunc(UsersService.search)
            } else {
                Pagination.setGetPageFunc(UsersService.getPage);
            }

            getPage(1, userInput, setPageData);
        }
    }
})();